# Matrix based message cryptor

##### TalTech IT College - Linear Algebra and Numerical Computations (I105)

This project contains of two java files:
* io.ehrbach.crypto.MatrixUtil
* io.ehrbach.crypto.CryptUtil

CryptUtil class encrypts and decrypts inputted string using [Hill cipher](https://en.wikipedia.org/wiki/Hill_cipher). MatrixUtil class provides static methods to support matrix operations. 

**DISCLAIMER:**

The code in this repository is made for educational purposes as a homework assignment. It should never be used in production environment for vital business operations. The author does not take any responsibility for possible issues that may arise from using this code.