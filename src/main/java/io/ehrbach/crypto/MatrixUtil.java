package io.ehrbach.crypto;

public class MatrixUtil {
    static double[][] getTransposed(double[][] src) {
        double[][] transposed = new double[src[0].length][src.length];

        for (int i = 0; i < src.length; i++) {
            for (int j = 0; j < src[i].length; j++) {
                transposed[j][i] = src[i][j];
            }
        }

        return transposed;
    }

    static double[][] getMinor(double[][] src, int excludedRow, int excludedColumn) {
        double[][] minor = new double[src.length - 1][src[0].length - 1];

        for (int i = 0; i < src.length; i++) {
            if (i == excludedRow) continue;
            for (int j = 0; j < src[i].length; j++) {
                if (j == excludedColumn) continue;
                minor[i < excludedRow ? i : i - 1][j < excludedColumn ? j : j - 1] = src[i][j];
            }
        }

        return minor;
    }

    static double getDeterminant(double[][] m) {
        if (m.length == 1) {
            return m[0][0];
        }

        if (m.length == 2) {
            return (m[0][0] * m[1][1]) - (m[0][1] * m[1][0]);
        }

        if (m.length == 3) {
            return m[0][0] * m[1][1] * m[2][2] + m[0][1] * m[1][2] * m[2][0] + m[0][2] * m[1][0] * m[2][1] -
                    m[0][1] * m[1][0] * m[2][2] - m[0][0] * m[1][2] * m[2][1] - m[0][2] * m[1][1] * m[2][0];
        }

        throw new UnsupportedOperationException("Maximum of 3x3 determinant allowed. Got: " + m.length + "x" + m.length);
    }

    static String getMatrixString(double[][] matrix) {
        StringBuilder sb = new StringBuilder();

        for (double[] row : matrix) {
            sb.append("|");
            for (int colId = 0; colId < row.length; colId++) {
                sb.append(row[colId]);
                if (colId + 1 != row.length) {
                    sb.append(" ");
                }
            }
            sb.append("|");
            sb.append(System.lineSeparator());
        }

        sb.append("Determinant: ").append(getDeterminant(matrix));

        return sb.toString();
    }
}
