package io.ehrbach.crypto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.util.Arrays;

public class CryptUtil {
    private static final Charset DEFAULT_CHARSET = Charset.forName("ISO-8859-4");
    private static final String FILLER = " ";
    private static final String SECRET = ".,123@&£s";
    private static final int THRESHOLD = 3;

    public static void main(String args[]) {
        String source = "Tere Tulemast Raekoja Platsile. Kohtume Toompeal kell 15.00!";

        while (source.length() % THRESHOLD != 0) {
            source = source.concat(FILLER);
        }

        byte[] sourceBytes = source.getBytes(DEFAULT_CHARSET);
        double[] crypted = encryptAndGetResult(sourceBytes);
        byte[] decryptedBytes = decryptAndGetResult(crypted);
//        System.out.println("Secret: \n" + MatrixUtil.getMatrixString(getSecret()));
//        System.out.println("Inverse of secret: \n" + MatrixUtil.getMatrixString(getSecretInverse()));
        System.out.println("Source string: " + source);
        System.out.println("Source bytes:" + Arrays.toString(sourceBytes));
        System.out.println("Out integers:" + Arrays.toString(crypted));
        System.out.println("Decrypted bytes: " + Arrays.toString(decryptedBytes));
        System.out.println("Decrypted string: " + new String(decryptedBytes, DEFAULT_CHARSET));

    }

    private static double[][] getSecretInverse() {
        double[][] src = getSecret();
        if (MatrixUtil.getDeterminant(src) == 0)
            throw new RuntimeException("Determinant of secret is 0. Can't inverse.");

        double[][] transposed = MatrixUtil.getTransposed(src);
        double[][] inverse = new double[src.length][src[0].length];
        double matrixScalar = 1 / MatrixUtil.getDeterminant(src);

        double nextDeterminantScalar = 1;
        for (int i = 0; i < inverse.length; i++) {
            for (int j = 0; j < inverse[i].length; j++) {
                double[][] minor = MatrixUtil.getMinor(transposed, i, j);
                double minorDeterminant = MatrixUtil.getDeterminant(minor);
                inverse[i][j] = matrixScalar * nextDeterminantScalar * minorDeterminant;
                nextDeterminantScalar *= (-1);
            }
        }

        return inverse;
    }

    private static byte[] decryptAndGetResult(double[] crypted) {
        double[][] inverseSecret = getSecretInverse();
        byte[] result = new byte[crypted.length];

        int fragIdx = 0;
        double[] partialSrc = new double[THRESHOLD];
        for (int i = 0; i < crypted.length; i++) {
            partialSrc[fragIdx] = crypted[i];

            if ((i + 1) % THRESHOLD == 0) {
                int srcIdx = i;
                double[] intermediate = new double[THRESHOLD];
                for (int j = inverseSecret.length - 1; j >= 0; j--) {
                    for (int k = 0; k < partialSrc.length; k++) {
                        intermediate[j] += partialSrc[k] * inverseSecret[j][k];
                    }
                    result[srcIdx] = (byte) (BigDecimal.valueOf(intermediate[j]).setScale(0, RoundingMode.HALF_UP).intValue());
                    srcIdx--;
                }
                fragIdx = 0;
            } else {
                fragIdx++;
            }
        }
        return result;
    }

    private static double[] encryptAndGetResult(byte[] src) {
        double[][] secret = getSecret();
        double[] result = new double[src.length];

        int fragIdx = 0;
        double[] partialSrc = new double[THRESHOLD];
        for (int i = 0; i < src.length; i++) {
            partialSrc[fragIdx] = src[i];

            if ((i + 1) % THRESHOLD == 0) {
                int srcIdx = i;
                double[] intermediate = new double[THRESHOLD];
                for (int j = secret.length - 1; j >= 0; j--) {
                    for (int k = 0; k < partialSrc.length; k++) {
                        intermediate[j] += partialSrc[k] * secret[j][k];
                    }
                    result[srcIdx] = intermediate[j];
                    srcIdx--;
                }
                fragIdx = 0;
            } else {
                fragIdx++;
            }
        }
        return result;
    }

    private static double[][] getSecret() {
        int requiredLen = (int) Math.pow(THRESHOLD, 2);
        if (SECRET.length() != requiredLen) {
            throw new IllegalArgumentException("Invalid secret. Length must be " + requiredLen + ". Got: " + SECRET.length());
        }

        byte[] bytes = SECRET.getBytes(DEFAULT_CHARSET);
        double[][] out = new double[THRESHOLD][THRESHOLD];
        int rowIdx = 0;
        int colIdx = 0;
        for (byte aByte : bytes) {
            out[rowIdx][colIdx] = aByte;
            colIdx++;
            if (colIdx / THRESHOLD == 1) {
                rowIdx++;
                colIdx = 0;
            }
        }
        return out;
    }


}
